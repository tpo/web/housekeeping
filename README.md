# Web Housekeeping

This is a collection of CI jobs intended to be run on a schedule.

It requires a valid API token from the `ci-housekeeper-bot` user account in the
`CI_HOUSEKEEPER_TOKEN` CI variable.

## undeployed-builds

This job iterates through the list of projects for which the
`ci-housekeeper-bot` has membership and checks if any of them have a pending
`deploy-prod` job. If it finds one, it will notify on IRC and, if possible,
leave a comment on the associated MR.

To enable this job to watch a given web project, simply add
`ci-housekeeper-bot` as a member with Guest permissions.
